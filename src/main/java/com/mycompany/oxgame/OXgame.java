/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 */

package com.mycompany.oxgame;

/**
 *
 * @author ATICHON
 */
import java.util.Scanner;

public class OXGame {
    private char[][] board;
    private char currentPlayer;

    public OXGame() {
        board = new char[3][3];
        currentPlayer = 'O';
        initializeBoard();
    }

    private void initializeBoard() {
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                board[i][j] = '-';
            }
        }
    }

    private void displayBoard() {
        System.out.println("-------------");
        for (int i = 0; i < 3; i++) {
            System.out.print("| ");
            for (int j = 0; j < 3; j++) {
                System.out.print(board[i][j] + " | ");
            }
            System.out.println("\n-------------");
        }
    }
    
    public boolean playAgain() {
        Scanner scanner = new Scanner(System.in);
        String input;
    
        while (true) {
            System.out.print("Do you want to play again? (Y/N): ");
            input = scanner.nextLine().trim().toUpperCase();
            if (input.equals("Y")) {
                return true;
            } else if (input.equals("N")) {
                return false;
            } else {
                System.out.println("Invalid input. Please enter Y or N.");
            }
        }
    }

    public void playGame() {
        boolean gameEnd = false;
        int row, col;
    
        while (!gameEnd) {
            displayBoard();
            System.out.println("Player " + currentPlayer + ", enter your move (row[1-3] column[1-3]):");
            row = getUserInput("row") - 1;
            col = getUserInput("column") - 1;
    
            if (isValidMove(row, col)) {
                board[row][col] = currentPlayer;
                if (isWinningMove(row, col)) {
                    displayBoard();
                    System.out.println("Player " + currentPlayer + " wins!");
                    gameEnd = true;
                } else if (isBoardFull()) {
                    displayBoard();
                    System.out.println("It's a draw!");
                    gameEnd = true;
                } else {
                    currentPlayer = (currentPlayer == 'O') ? 'X' : 'O';
                }
            } else {
                System.out.println("Invalid move. Please try again.");
            }
        }
    
        if (playAgain()) {
            initializeBoard();
            currentPlayer = 'O';
            playGame();
        }
    }
    

    private int getUserInput(String coordinate) {
        Scanner scanner = new Scanner(System.in);
        int input;

        while (true) {
            System.out.print("Enter " + coordinate + " : ");
            if (scanner.hasNextInt()) {
                input = scanner.nextInt();
                if (input >= 1 && input <= 3) {
                    break;
                }
            } else {
                scanner.next();
            }
            System.out.println("Invalid input. Please enter a number from 1 to 3.");
        }

        return input;
    }

    private boolean isValidMove(int row, int col) {
        if (row >= 0 && row < 3 && col >= 0 && col < 3 && board[row][col] == '-') {
            return true;
        }
        return false;
    }

    private boolean isWinningMove(int row, int col) {
        // Check row
        if (board[row][0] == board[row][1] && board[row][1] == board[row][2]) {
            return true;
        }

        // Check column
        if (board[0][col] == board[1][col] && board[1][col] == board[2][col]) {
            return true;
        }

        // Check diagonal
        if (board[0][0] == board[1][1] && board[1][1] == board[2][2] && (row == col || row + col == 2)) {
            return true;
        }

        return false;
    }

    private boolean isBoardFull() {
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                if (board[i][j] == '-') {
                    return false;
                }
            }
        }
        return true;
    }

    public static void main(String[] args) {
        OXGame game = new OXGame();
        game.playGame();
    }
}



    

  